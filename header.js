let prevScrollpos = window.pageYOffset;
window.onscroll = function() {
    let currentScrollPos = window.pageYOffset;
    if (prevScrollpos > currentScrollPos || currentScrollPos < 55 || screen.availWidth >= 775) {
        document.querySelector(".header").style.top = "0";
    } else {
        document.querySelector(".header").style.top = "-110px";
    }
    prevScrollpos = currentScrollPos;
}