document.querySelectorAll('.preference').forEach(elem => {
    elem.querySelector('.preference__fold').addEventListener('click', (e) => {
        elem.classList.toggle('folded');
    })
})

document.querySelectorAll('.worker__button').forEach(elem => {
    elem.onclick = () => {
        document.querySelector('.contact_modal').classList.toggle('hidden');
    }
})

document.querySelector('.contact_modal__close').onclick = () => {
    document.querySelector('.contact_modal').classList.toggle('hidden');
}

document.querySelector('.goto_top').onclick = () => {
    window.scrollTo(0, 0);
}

function asideOpen() {
    document.querySelector('.left_aside').classList.remove('hidden');
    document.body.style.position = 'fixed';
    document.body.style.top = `-${window.scrollY}px`;
}

function asideClose() {
    document.querySelector('.left_aside').classList.add('hidden');
    document.body.style.position = '';
    document.body.style.top = '';
}

document.querySelector('.left_aside__fold').onclick = asideClose;
document.querySelector('.left_aside__open').onclick = asideOpen;


window.addEventListener('scroll', () => {
    document.querySelector('.goto_top').className = window.pageYOffset > 100 ? 'goto_top' : 'goto_top hidden';
})